package br.com.jeyson.agilsocorro.model;

import android.util.Log;

import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * Created by Paulo on 09.08.2016.
 */
public class Usuario implements Serializable {

    private Long id;
    private String nomeLogin;
    private String telefone;
    private char ativo;

    public char getAtivo() {
        return ativo;
    }

    public void setAtivo(char ativo) {
        this.ativo = ativo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNomeLogin() {
        return nomeLogin;
    }

    public void setNomeLogin(String nomeLogin) {
        this.nomeLogin = nomeLogin;
    }

    public void setId(Long id){
        this.id=id;
    }

    public Long getId() {
        return id;
    }

    public Usuario(){}
    public Usuario(String nomelogin, String telefone, char ativo){
        this.nomeLogin=nomelogin;
        this.telefone=telefone;
        this.ativo=ativo;
    }



}
