package br.com.jeyson.agilsocorro.model;

import java.util.List;

/**
 * Created by Jeyson on 15/08/2016.
 */
public class CategoriaLista {

    private List<Categoria> categorias;

    public List<Categoria> getCategorias ()
    {
        return categorias;
    }

    public void setCategorias (List<Categoria> categorias)
    {
        this.categorias = categorias;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [categorias = "+categorias+"]";
    }
}
