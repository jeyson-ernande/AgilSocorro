package br.com.jeyson.agilsocorro.negocio;

import android.content.ContentValues;

import br.com.jeyson.agilsocorro.model.Paciente;
import br.com.jeyson.agilsocorro.model.Usuario;

/**
 * Created by Paulo on 09.08.2016.
 * Classe Util para parse de values para o banco de dados SQLite.
 */
public class Util {
    /*
    * @Object: Objeto que se deseja persistir.
    * Método responsável por auxiliar na inserção de objetos no banco de dados.
    * */
    public static ContentValues toValues(Object object){
        ContentValues values = new ContentValues();
        if(object instanceof Usuario) {
            Usuario login = (Usuario) object;
            values.put(Constantes.USUARIO_LOGIN_COLUNA_NOME, login.getNomeLogin());
            values.put(Constantes.USUARIO_LOGIN_COLUNA_TELEFONE, login.getTelefone());
            values.put(Constantes.USUARIO_LOGIN_COLUNA_ATIVO, String.valueOf(login.getAtivo()));
        }
        if(object instanceof Paciente){
            Paciente paciente = (Paciente)object;
            values.put(Constantes.PACIENTE_NOME_COLUNA,paciente.getNome());
            values.put(Constantes.PACIENTE_IDADE_COLUNA,paciente.getIdade());
            values.put(Constantes.PACIENTE_SEXO_COLUNA,String.valueOf(paciente.getSexo()));
        }
        return  values;
    }
}
