package br.com.jeyson.agilsocorro.model;

import java.io.Serializable;

/**
 * Created by Pedro Bittar on 12/08/2016.
 * Edited by Paulo on 17.08.2016
 * sexo attribut included
 * Serializable implementation
 * int attribute of codigo switched to Long
 */
public class Paciente implements Serializable {
    private Long codigo;
    private Long idUsuario;
    private String nome;
    private int idade;
    private char sexo;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public char getSexo() {return sexo;}

    public void setSexo(char sexo) {this.sexo = sexo;}

    public Long getIdUsuario() {return idUsuario;}

    public void setIdUsuario(Long idUsuario) {this.idUsuario = idUsuario;}

    public Paciente(){}

    public Paciente(String nome, int idade,char sexo){
        this.nome = nome;
        this.idade = idade;
        this.sexo=sexo;
    }

    public Paciente(Long codigo, String nome, int idade,char sexo){
        this.codigo = codigo;
        this.nome = nome;
        this.idade = idade;
        this.sexo=sexo;
    }

}
