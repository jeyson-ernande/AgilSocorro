package br.com.jeyson.agilsocorro.dao;

import android.content.Context;

import java.util.List;

import br.com.jeyson.agilsocorro.model.Paciente;

/**
 * Created by Paulo on 17.08.2016.
 */
public interface IPacienteSQLiteDAO {
    void insert(Paciente novoPaciente, Context ctx);
    void delete(Paciente paciente,Context ctx);
    List<Paciente> listarPaciente(Context ctx);
    Long selectPacienteID(Paciente paciente,Context ctx);
}
