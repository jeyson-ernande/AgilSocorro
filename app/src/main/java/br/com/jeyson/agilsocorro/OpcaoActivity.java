package br.com.jeyson.agilsocorro;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.jeyson.agilsocorro.model.Questionario;
import br.com.jeyson.agilsocorro.negocio.Constantes;


/**
 * Created by Pedro Bittar on 12/08/2016.
 */
public class OpcaoActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private Button btnPedirSocorro;
    private Button btnListarPacientes;
    private Questionario questionario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcao);

        btnPedirSocorro = (Button)findViewById(R.id.buttonSamu);
        btnListarPacientes = (Button)findViewById(R.id.buttonLista);

        btnPedirSocorro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                /*CategoriaActivity.class*/
                Intent it = new Intent(getApplicationContext(), CategoriaActivity.class);
                startActivity(it);
            }
        });
        btnListarPacientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it;
                it = new Intent(getApplicationContext(), ListarPacienteActivity.class);
                startActivity(it);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_menu,menu);


        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent abrirTelaCasdastroPaciente = new Intent(OpcaoActivity.this,CadastroPacienteActivity.class);
                startActivity(abrirTelaCasdastroPaciente);
                return  false;
            }
        });
        menu.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(OpcaoActivity.this,R.string.mensagem_recurso_indisponivel,Toast.LENGTH_LONG).show();
                return false;
            }
        });
        return true;
    }
}
