package br.com.jeyson.agilsocorro.dao;

import android.content.Context;

import java.util.List;

import br.com.jeyson.agilsocorro.model.Usuario;

/**
 * Created by Paulo on 09.08.2016.
 */
public interface ILoginSQLiteDAO {
    void insert(Usuario novoLogin,Context ctx);
    void delete(Usuario login,Context ctx);
    List<Usuario> listarLogin(Context ctx);
    Long buscarID(String numeroTelefone,Context ctx);
}
