package br.com.jeyson.agilsocorro.model;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Pedro Bittar on 18/08/2016.
 */
public class Questionario implements Serializable {
    private String categoria;
    private Boolean consciente;
    private Boolean sangrando;


    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Boolean getConsciente() {
        return consciente;
    }

    public void setConsciente(Boolean consciente) {
        this.consciente = consciente;
    }

    public Boolean getSangrando() {
        return sangrando;
    }

    public void setSangrando(Boolean sangrando) {
        this.sangrando = sangrando;
    }

    public Questionario(){}

    public Questionario(String categoria, Boolean consciente, Boolean sangrando){
        this.categoria = categoria;
        this.consciente = consciente;
        this.sangrando = sangrando;
    }

    @Override
    public String toString() {
        return categoria;
    }
}
