package br.com.jeyson.agilsocorro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import br.com.jeyson.agilsocorro.R;
import br.com.jeyson.agilsocorro.model.Paciente;

/**
 * Created by Pedro Bittar on 14/08/2016.
 */
public class PacienteAdapter extends BaseAdapter {

    private List<Paciente> pacienteList;
    private Context context;

    public PacienteAdapter(Context ctx, List<Paciente> pacientes) {
        context = ctx;
        pacienteList = pacientes;
    }

    @Override
    public int getCount() {

        return pacienteList.size();
    }

    @Override
    public Object getItem(int position) {

        return pacienteList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Paciente paciente = pacienteList.get(position);
        ViewHolder holder = null;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.fragment_paciente, null);
            holder = new ViewHolder();
            holder.txtNome = (TextView) convertView.findViewById(R.id.nome);
            holder.txtIdade = (TextView) convertView.findViewById(R.id.idade);
            holder.txtSexo = (TextView) convertView.findViewById(R.id.sexo);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.txtNome.setText(paciente.getNome());
        holder.txtIdade.setText(Integer.toString(paciente.getIdade()));
        holder.txtSexo.setText(Character.toString(paciente.getSexo()));

        return convertView;
    }

    static class ViewHolder{
        TextView txtNome;
        TextView txtIdade;
        TextView txtSexo;
    }
}
