package br.com.jeyson.agilsocorro;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import br.com.jeyson.agilsocorro.adapter.CategoriaAdapter;
import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.util.Utils;

import br.com.jeyson.agilsocorro.model.Categoria;
import br.com.jeyson.agilsocorro.model.CategoriaLista;

public class CategoriaActivity extends AppCompatActivity {

    private Gson mGson;

    private static List<Categoria> CATEGORIAS;

    private static String TAG = "Erro Conexao";

    private ProgressDialog mProgress;

    private CategoriaAdapter mCategoriaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categiria_activity);
        new SyncDataTask().execute();

    }

    private void loadData() {

        try {
            mGson = new Gson();

            CategoriaLista listaCategorias;

            URL url = new URL(Utils.sURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10 * 1000);
            connection.setConnectTimeout(15 * 1000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                final BufferedReader reader =
                        new BufferedReader(new InputStreamReader(connection.getInputStream()));
                listaCategorias = mGson.fromJson(reader, CategoriaLista.class);

                this.CATEGORIAS = listaCategorias.getCategorias();

            } else {
                Log.i(TAG, "Erro de Conexao");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupListView() {

        mCategoriaAdapter = new CategoriaAdapter(this, CATEGORIAS);
        ListView listaCategoriasView = (ListView) findViewById(R.id.lista_categorias);
        listaCategoriasView.setAdapter(mCategoriaAdapter);

        listaCategoriasView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Categoria categoria = (Categoria) adapterView.getItemAtPosition(i);

                Intent it = new Intent(CategoriaActivity.this, PerguntaActivity.class);
                it.putExtra(Constantes.EXTRA_CATEGORIA, categoria.getNome());

                startActivity(it);
            }
        });

    }

    private class SyncDataTask extends AsyncTask<Object, Object, Object> {

        @Override
        protected void onPreExecute() {

            mProgress = new ProgressDialog(CategoriaActivity.this);
            mProgress.setTitle(R.string.download_json);
            mProgress.setMessage(getString(R.string.carregando));
            mProgress.show();

        }

        @Override
        protected Object doInBackground(Object... params) {

            loadData();
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {

            if (mProgress != null && mProgress.isShowing()) {
                try {
                    mProgress.dismiss();
                } catch (Exception e) {

                }
            }
            setupListView();

        }
    }

}
