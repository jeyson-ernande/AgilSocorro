package br.com.jeyson.agilsocorro.dao;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.jeyson.agilsocorro.model.Paciente;
import br.com.jeyson.agilsocorro.model.Usuario;
import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.negocio.Util;

/**
 * Created by Paulo on 17.08.2016.
 */
public class PacienteSQLiteDAO {

    private final DataBaseHelper helper;

    public PacienteSQLiteDAO(DataBaseHelper helper){
        this.helper=helper;
    }

    public void insert(Paciente novoPaciente){
        helper.getWritableDatabase().insert(helper.PACIENTE_TABLE,null,  Util.toValues(novoPaciente));
        Log.i("GRAVADO","Gravei o Paciente"+novoPaciente.getNome().toString());
        helper.close();;
    }
    public void delete(Paciente paciente, Context ctx){
        String args[]={paciente.getCodigo().toString()};
        helper.getWritableDatabase().delete(helper.PACIENTE_TABLE, Constantes.PACIENTE_ID_COLUNA+"=?",args);
        helper.close();
    }

    public List<Paciente> listarPaciente() {
        List<Paciente> lstPaciente = new ArrayList<Paciente>();
        String sql = "SELECT * FROM "+helper.PACIENTE_TABLE+";";

        Cursor curs=helper.getReadableDatabase().rawQuery(sql,null);
        if(curs!=null & curs.getCount()>0) {
                while (curs.moveToNext()) {
                    Paciente paciente = new Paciente();
                    paciente.setCodigo(curs.getLong(curs.getColumnIndex(Constantes.PACIENTE_ID_COLUNA)));
                    paciente.setNome(curs.getString(curs.getColumnIndex(Constantes.PACIENTE_NOME_COLUNA)));
                    paciente.setIdade(curs.getInt(curs.getColumnIndex(Constantes.PACIENTE_IDADE_COLUNA)));
                    String sexo=curs.getString(curs.getColumnIndex(Constantes.PACIENTE_SEXO_COLUNA));
                    paciente.setSexo(sexo.charAt(0));
                    lstPaciente.add(paciente);
                }
                helper.close();
        }
        return lstPaciente;
    }
    public Long selectPacienteID(Paciente pac,Context ctx){
        Long id=null;
        String query="SELECT "+Constantes.PACIENTE_ID_COLUNA+
                     " FROM "+helper.PACIENTE_TABLE+
                     " WHERE "+Constantes.PACIENTE_NOME_COLUNA+"="+pac.getNome()+"" +
                     " AND "+Constantes.PACIENTE_IDADE_COLUNA+"="+pac.getIdade()+";";
        Cursor curs = helper.getReadableDatabase().rawQuery(query,null);
        if(curs!=null&&curs.getCount()>0){
            while (curs.moveToNext()) {
                id = curs.getLong(curs.getColumnIndex(Constantes.PACIENTE_ID_COLUNA));
            }
        }
        return id;
    }

}
