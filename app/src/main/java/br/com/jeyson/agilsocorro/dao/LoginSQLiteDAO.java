package br.com.jeyson.agilsocorro.dao;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.jeyson.agilsocorro.model.Usuario;
import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.negocio.Util;
/**
 * Created by Paulo on 09.08.2016.
 */
public class LoginSQLiteDAO {
    private final DataBaseHelper helper;

    public LoginSQLiteDAO(DataBaseHelper helper){
        this.helper=helper;
    }

    public void insert(Usuario novoLogin){
        helper.getWritableDatabase().insert(helper.USUARIO_TABLE,null,  Util.toValues(novoLogin));
        Log.i("GRAVADO","Gravei o usuario");
        helper.close();;
    }
    public void delete(Usuario login, Context ctx){
        String args[]={login.getId().toString()};
        helper.getWritableDatabase().delete(helper.USUARIO_TABLE,Constantes.USUARIO_LOGIN_COLUNA_ID+"=?",args);
        helper.close();
    }

    public List<Usuario> listarLogin() {
        List<Usuario> logins = null;//new ArrayList<Usuario>();
        String sql = "SELECT * FROM "+helper.USUARIO_TABLE+" WHERE ID IS NOT NULL;";

        Cursor curs=helper.getReadableDatabase().rawQuery(sql,null);
        if(curs!=null & curs.getCount()>0) {
            if (curs.moveToFirst()) {
                while (curs.moveToNext()) {
                    Usuario login = new Usuario();
                    logins=new ArrayList<Usuario>();
                    login.setId(curs.getLong(curs.getColumnIndex(Constantes.USUARIO_LOGIN_COLUNA_ID)));
                    login.setNomeLogin(curs.getString(curs.getColumnIndex(Constantes.USUARIO_LOGIN_COLUNA_NOME)));
                    login.setTelefone(curs.getString(curs.getColumnIndex(Constantes.USUARIO_LOGIN_COLUNA_TELEFONE)));

                    logins.add(login);
                }
                helper.close();
            }
        }
        return logins;
    }
    public Long buscarID(String numeroTelefone){
        Long id=null;
        String query="SELECT "+Constantes.USUARIO_LOGIN_COLUNA_ID+
                " FROM "+helper.USUARIO_TABLE+
                " WHERE "+Constantes.USUARIO_LOGIN_COLUNA_TELEFONE+"="+numeroTelefone+";";
        Cursor curs = helper.getReadableDatabase().rawQuery(query,null);
        if(curs!=null&&curs.getCount()>0){
            while (curs.moveToNext()) {
                id = curs.getLong(curs.getColumnIndex(Constantes.PACIENTE_ID_COLUNA));
            }
        }
        return id;
    }
}
