package br.com.jeyson.agilsocorro.negocio;

/**
 * Created by Paulo on 09.08.2016.
 */
public class Constantes {
    /*Constantes referente a estória LOGIN*/
    public static  final char USARIO_ATIVO_SIM='S';
    public static final char USUARIO_ATIVO_NAO='N';
    public static final String USUARIO_LOGIN_COLUNA_ID="ID";
    public static final String USUARIO_LOGIN_COLUNA_TELEFONE="TELEFONE";
    public static final String USUARIO_LOGIN_COLUNA_NOME="NOME";
    public static final String USUARIO_LOGIN_COLUNA_ATIVO="ATIVO";
    public static final String TELEFONE_LOGADO="TELEFONE LOGADO";
    public static final String EXTRA_CATEGORIA = "categoria";

    /*Constantes referentes a estória PACIENTES*/
    public static final String PACIENTE_ID_COLUNA="ID";
    public static final String PACIENTE_USUARIO_ID_COLUNA_FK="ID_USUARIO";
    public static final String PACIENTE_NOME_COLUNA="NOMEPACIENTE";
    public static final String PACIENTE_IDADE_COLUNA="IDADE";
    public static final String  PACIENTE_SEXO_COLUNA="SEXO";
    public static final char PACIENTE_SEXO_MASCULINO='M';
    public static final char PACIENTE_SEXO_FEMININO='F';

    /*Constantes globalization*/
    public static final String BRASIL="BR";


}
