package br.com.jeyson.agilsocorro.negocio;

import android.content.Context;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.widget.EditText;

import java.util.List;

import br.com.jeyson.agilsocorro.model.Usuario;
import br.com.jeyson.agilsocorro.dao.DataBaseHelper;
import br.com.jeyson.agilsocorro.dao.ILoginSQLiteDAO;
import br.com.jeyson.agilsocorro.dao.LoginSQLiteDAO;

/**
 * Created by Paulo on 09.08.2016.
 */
public class UsuarioLoginNegocio implements ILoginSQLiteDAO {
    private DataBaseHelper dbHelper;
    private LoginSQLiteDAO dao;

    @Override
    public void insert(Usuario novoLogin,Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new LoginSQLiteDAO(dbHelper);
        dao.insert(novoLogin);
    }

    @Override
    public void delete(Usuario login,Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new LoginSQLiteDAO(dbHelper);
        dao.delete(login,ctx);
    }

    @Override
    public List<Usuario> listarLogin(Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new LoginSQLiteDAO(dbHelper);
        List<Usuario>l=dao.listarLogin();
        return l;
    }

    @Override
    public Long buscarID(String numeroTelefone,Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new LoginSQLiteDAO(dbHelper);
        return dao.buscarID(numeroTelefone);
    }

    public boolean isLoginValido(Context ctx){
        try {
            if (listarLogin(ctx).get(0).getId()!= null) {
                return true;
            } else {
                return false;
            }
        }catch (NullPointerException e){
            return  false;
        }
    }

    public String formatarNumeroRegiaoComNove(String numero){
        String tmp = new String();
        if(numero!=null){
            while (numero.length()<11){
                tmp.concat(numero);
            }
            if(numero.trim().length()==11){
                String [] numeroTmp = new String[numero.length()];
                numeroTmp[0]=numero.substring(0,1);
                numeroTmp[1]=numero.substring(2);
                numeroTmp[3]=numero.substring(3,6);
                numeroTmp[4]=numero.substring(7,10);
            }
        }
        return numero;
    }
}
