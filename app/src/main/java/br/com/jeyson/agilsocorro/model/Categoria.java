package br.com.jeyson.agilsocorro.model;

/**
 * Created by Jeyson on 15/08/2016.
 */
public class Categoria {

    public String nome;

    public Categoria(){

    }

    public Categoria(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }
}
