package br.com.jeyson.agilsocorro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.jeyson.agilsocorro.R;
import br.com.jeyson.agilsocorro.model.Categoria;

/**
 * Created by Jeyson on 20/08/2016.
 */
public class CategoriaAdapter extends BaseAdapter{

    private List<Categoria> categoriaList;
    private Context context;

    public CategoriaAdapter(Context ctx, List<Categoria> categorias) {
        context = ctx;
        categoriaList = categorias;
    }

    @Override
    public int getCount() {

        return categoriaList.size();
    }

    @Override
    public Object getItem(int position) {

        return categoriaList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Categoria categoria = categoriaList.get(position);
        ViewHolder holder = null;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.fragment_categoria, null);
            holder = new ViewHolder();
            holder.txtNome = (TextView) convertView.findViewById(R.id.nome_categoria);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.txtNome.setText(categoria.getNome());

        return convertView;
    }

    static class ViewHolder{
        TextView txtNome;
        TextView txtIdade;
    }
}
