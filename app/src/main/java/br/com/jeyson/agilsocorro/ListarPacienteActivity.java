package br.com.jeyson.agilsocorro;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.jeyson.agilsocorro.adapter.PacienteAdapter;
import br.com.jeyson.agilsocorro.model.Paciente;

import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.negocio.PacienteNegocio;


public class ListarPacienteActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    PacienteListFragment listFragment;
    List<Paciente> pacienteList;
    PacienteAdapter pacienteAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_paciente);

        pacienteList = new ArrayList<Paciente>();

        PacienteNegocio pacienteNegocio = new PacienteNegocio();
        pacienteList = pacienteNegocio.listarPaciente(ListarPacienteActivity.this);

        listView = (ListView)findViewById(R.id.fragment_lista);
        if(pacienteList!=null){
            pacienteAdapter = new PacienteAdapter(this, pacienteList);
            listView.setAdapter(pacienteAdapter);
        }

    }
}
