package br.com.jeyson.agilsocorro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import br.com.jeyson.agilsocorro.model.Questionario;

public class PerguntaFragment extends Fragment implements View.OnClickListener {

    Button btnSim;
    Button btnNao;
    TextView textView;
    FragmentManager fragmentManager;
    Questionario questionario;
    int cont;

    public PerguntaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        cont = args.getInt("cont");

        questionario = new Questionario();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_pergunta, container, false);
        btnSim = (Button) layout.findViewById(R.id.buttonSim);
        btnNao = (Button) layout.findViewById(R.id.buttonNao);
        btnSim.setOnClickListener(this);
        btnNao.setOnClickListener(this);
        textView = (TextView) layout.findViewById(R.id.textViewPergunta);
        textView.setText(R.string.pergunta1);


        return layout;
    }

    @Override
    public void onClick(View v) {
        Log.d("ai", "Entrou no OnClick");
        if (v == btnSim) {
            Log.d("SIM", "Entrou no OnClick do SIM");
            switch (cont) {
                case 0:
                    questionario.setConsciente(true);
                    cont++;
                    textView.setText(R.string.pergunta2);
                    break;
                case 1:
                    questionario.setSangrando(true);
                    cont++;
                    Log.d("B", questionario.getSangrando().toString());
                    //chama resultado activity
                    Intent it = new Intent(getActivity().getApplicationContext(), ConfirmacaoActivity.class);
                    startActivity(it);
                    break;
            }
        }
        if (v == btnNao) {
            Log.d("NAO", "Entrou no OnClick do NAO");
            switch (cont) {
                case 0:
                    questionario.setConsciente(false);
                    cont++;
                    textView.setText(R.string.pergunta2);
                    break;
                case 1:
                    questionario.setSangrando(false);
                    cont++;
                    //chama resultado activity
                    Intent it = new Intent(getActivity().getApplicationContext(), ConfirmacaoActivity.class);
                    startActivity(it);
                    break;
            }
        }
    }
}
