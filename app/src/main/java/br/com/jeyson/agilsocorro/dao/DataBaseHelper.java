package br.com.jeyson.agilsocorro.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.com.jeyson.agilsocorro.negocio.Constantes;

/**
 * Created by Paulo on 09.08.2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE="AgilSocorroDB";
    private static final int VERSAO=3;
    public static final String USUARIO_TABLE="USUARIO";
    public  static final String PACIENTE_TABLE="PACIENTE";

    public DataBaseHelper(Context contexto){
        super(contexto,DATABASE,null,VERSAO);
        Log.i("BANCO DE DADOS","Criado!");
    }

    @Override
    public void onCreate(SQLiteDatabase bancoSQLite){
        String query="CREATE TABLE "+USUARIO_TABLE+
                " ("+Constantes.USUARIO_LOGIN_COLUNA_ID+" INTEGER PRIMARY KEY," +
                Constantes.USUARIO_LOGIN_COLUNA_NOME+" STRING,"+
                Constantes.USUARIO_LOGIN_COLUNA_TELEFONE+" STRING UNIQUE NOT NULL,"+
                Constantes.USUARIO_LOGIN_COLUNA_ATIVO+" TEXT(1) NOT NULL);";
        bancoSQLite.execSQL(query);
        criarTabelaPaciente(bancoSQLite);

    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int versao,int versaoMaisUm1){
        //Log.i("BANCO DE DADOS!","Vou atualizar!");
        /*Código responsável para atualizar o banco de dados.*/
       /*String query="DROP TABLE IF EXISTS "+ USUARIO_TABLE;
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);
        Log.i("BANCO DE DADOS!","um novo banco foi craido!");*/
        criarTabelaPaciente(sqLiteDatabase);
    }
    protected void criarTabelaPaciente(SQLiteDatabase bancoSQLite){
        String  query="CREATE TABLE "+PACIENTE_TABLE+
                "("+Constantes.PACIENTE_ID_COLUNA+" INTEGER PRIMARY KEY," +
                Constantes.PACIENTE_USUARIO_ID_COLUNA_FK+" INTEGER,"+
                Constantes.PACIENTE_NOME_COLUNA+" STRING,"+
                Constantes.PACIENTE_IDADE_COLUNA+" INTEGER,"+
                Constantes.PACIENTE_SEXO_COLUNA+" TEXT(1)," +
                " FOREIGN KEY("+Constantes.PACIENTE_USUARIO_ID_COLUNA_FK+") REFERENCES "+USUARIO_TABLE+" ("+Constantes.USUARIO_LOGIN_COLUNA_ID+"));";
        Log.i("BANCO",query);
        bancoSQLite.execSQL(query);
        Log.i("BANCO","ATUALIZADO");
    }

}
