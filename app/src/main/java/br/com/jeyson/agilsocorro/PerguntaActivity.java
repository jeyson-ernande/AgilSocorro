package br.com.jeyson.agilsocorro;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import br.com.jeyson.agilsocorro.model.Questionario;


public class PerguntaActivity extends FragmentActivity {


    private FragmentManager fragmentManager;
    private PerguntaFragment perguntaFragment;
    private Questionario questionario = new Questionario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta);

        Bundle param = this.getIntent().getExtras();
        if (param != null) {
            questionario.setCategoria(param.getString("categoria"));
        }

        //perguntaFragment = new PerguntaFragment();
        //fragmentManager = getSupportFragmentManager();
        //perguntaFragment = (PerguntaFragment)fragmentManager.findFragmentById(R.id.);

        Bundle args = new Bundle();
        args.putInt("cont", 0);
        args.putSerializable("quest",questionario);


        perguntaFragment = new PerguntaFragment();
        perguntaFragment.setArguments(args);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame, perguntaFragment);
        ft.commit();
    }
}
