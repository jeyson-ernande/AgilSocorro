package br.com.jeyson.agilsocorro.util;

import java.util.regex.Pattern;

/**
 * Created by Paulo on 17.08.2016.
 */
public class Validador {
    public static boolean somenteTexto(String texto){
        String exp="[a-zA-Z]";
        return Pattern.matches(exp,texto);
    }
}
