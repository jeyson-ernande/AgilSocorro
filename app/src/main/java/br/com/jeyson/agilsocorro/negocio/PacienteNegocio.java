package br.com.jeyson.agilsocorro.negocio;

import android.content.Context;

import java.util.List;

import br.com.jeyson.agilsocorro.dao.DataBaseHelper;
import br.com.jeyson.agilsocorro.dao.IPacienteSQLiteDAO;
import br.com.jeyson.agilsocorro.dao.PacienteSQLiteDAO;
import br.com.jeyson.agilsocorro.model.Paciente;

/**
 * Created by Paulo on 17.08.2016.
 */
public class PacienteNegocio implements IPacienteSQLiteDAO {
    private DataBaseHelper dbHelper;
    private PacienteSQLiteDAO dao;


    @Override
    public void insert(Paciente novoPaciente, Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new PacienteSQLiteDAO(dbHelper);
        dao.insert(novoPaciente);
    }

    @Override
    public void delete(Paciente paciente, Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new PacienteSQLiteDAO(dbHelper);
        dao.delete(paciente,ctx);

    }

    @Override
    public List<Paciente> listarPaciente(Context ctx) {
        dbHelper=new DataBaseHelper(ctx);
        dao=new PacienteSQLiteDAO(dbHelper);
        return dao.listarPaciente();
    }

    @Override
    public Long selectPacienteID(Paciente paciente, Context ctx) {
        return null;
    }
}
