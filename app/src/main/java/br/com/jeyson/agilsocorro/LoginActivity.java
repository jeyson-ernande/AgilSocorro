package br.com.jeyson.agilsocorro;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.jeyson.agilsocorro.model.Usuario;
import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.negocio.UsuarioLoginNegocio;

public class LoginActivity extends Activity {
    private UsuarioLoginNegocio negocioUsuario = new UsuarioLoginNegocio();
    private List<Usuario> listaLogin=new ArrayList<Usuario>();
    Intent abrirTela2;
    private EditText campoTexto;
    private EditText campoTelefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final Button btnLogin=(Button)findViewById(R.id.btnLogin);
        campoTelefone = (EditText) findViewById(R.id.txtTelefone);
        campoTelefone.addTextChangedListener(new PhoneNumberFormattingTextWatcher(Constantes.BRASIL));


        campoTelefone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            //String z=negocioUsuario.formatarNumeroRegiaoComNove(campoTelefone.getText().toString());
                String numeroDoTelefone= campoTelefone.getText().toString();
                if(numeroDoTelefone.length()==2){
                    numeroDoTelefone=numeroDoTelefone+" ";
                }
                if(numeroDoTelefone.trim().indexOf(2)==9){
                    numeroDoTelefone=numeroDoTelefone+" ";
                }
                //String numeroDoTelefoneTmp=String.valueOf(android.telephony.PhoneNumberUtils.formatNumber(numeroDoTelefone));

               // campoTelefone.setText(negocioUsuario.formatarTelefone(campoTelefone).getText());
                //campoTelefone.setSelection(numeroDoTelefoneTmp.length());
                return false;
            };
        });

        abrirTela2 = new Intent(LoginActivity.this, OpcaoActivity.class);

        if(negocioUsuario.isLoginValido(LoginActivity.this)){
            List<Usuario>lst=negocioUsuario.listarLogin(LoginActivity.this);
            abrirTela2.putExtra(Constantes.TELEFONE_LOGADO,lst.get(0).getTelefone());
            startActivity(abrirTela2);
        }else{
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    campoTexto = (EditText) findViewById(R.id.txtNome);
                    campoTelefone =(EditText)findViewById(R.id.txtTelefone);
                    Usuario usuario = new Usuario(); //usuarioDaActivity.pegaAlunoDoFormulario(LoginActivity.this);
                    if(!campoTexto.getText().toString().equals("") || !campoTelefone.getText().toString().equals("") ) {
                        usuario.setNomeLogin(campoTexto.getText().toString());
                        usuario.setTelefone(campoTelefone.getText().toString());
                        //usuario.setAtivo(Constantes.USARIO_ATIVO_SIM);
                        negocioUsuario.insert(usuario, LoginActivity.this);
                        abrirTela2.putExtra(Constantes.TELEFONE_LOGADO, usuario.getTelefone());
                        startActivity(abrirTela2);
                    }else{
                        Toast.makeText(LoginActivity.this,R.string.mensagem_campo_vazio, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
