package br.com.jeyson.agilsocorro;

import br.com.jeyson.agilsocorro.model.Paciente;

/**
 * Created by Pedro Bittar on 14/08/2016.
 */
public interface OnPacienteSelectedListener {

    void OnPacienteSelected(Paciente paciente);
}
