package br.com.jeyson.agilsocorro;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import br.com.jeyson.agilsocorro.model.Paciente;
import br.com.jeyson.agilsocorro.negocio.Constantes;
import br.com.jeyson.agilsocorro.negocio.PacienteNegocio;
import br.com.jeyson.agilsocorro.negocio.UsuarioLoginNegocio;

public class CadastroPacienteActivity extends Activity {
    private PacienteNegocio negocioPaciente = new PacienteNegocio();
    private UsuarioLoginNegocio negocioUsuario = new UsuarioLoginNegocio();

    private EditText txtNome;
    private EditText txtIdade;
    private RadioButton rbtMasculino;
    private RadioButton rbtFeminino;
    private Paciente novoPaciente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastropaciente);
        Button btnSalvar=(Button)findViewById(R.id.btnSalvar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNome=(EditText)findViewById(R.id.txtNomePaciente);
                txtIdade=(EditText)findViewById(R.id.txtIdadePaciente);
                rbtMasculino=(RadioButton)findViewById(R.id.rbtMasculino);
                rbtFeminino=(RadioButton)findViewById(R.id.rbtFeminino);
                novoPaciente=new Paciente();
                novoPaciente.setNome(txtNome.getText().toString());
                novoPaciente.setIdUsuario(Long.parseLong("1"));
                Long idUsuarioLogado=negocioUsuario.buscarID((String)getIntent().getSerializableExtra(Constantes.TELEFONE_LOGADO),getApplicationContext());
                novoPaciente.setIdUsuario(idUsuarioLogado);
                novoPaciente.setIdade(Integer.parseInt(txtIdade.getText().toString()));
                if(rbtMasculino.isChecked()){
                    novoPaciente.setSexo(Constantes.PACIENTE_SEXO_MASCULINO);
                }else{
                    novoPaciente.setSexo(Constantes.PACIENTE_SEXO_FEMININO);
                }
                negocioPaciente.insert(novoPaciente,CadastroPacienteActivity.this);
                Intent abrirOpcao = new Intent(CadastroPacienteActivity.this, OpcaoActivity.class);
                startActivity(abrirOpcao);
                //List<Paciente> pacienteList = negocioPaciente.listarPaciente(getApplicationContext());

            }
        });
    }
}
